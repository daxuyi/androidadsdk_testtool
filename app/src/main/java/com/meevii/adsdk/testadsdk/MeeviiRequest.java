package com.meevii.adsdk.testadsdk;

import android.content.Context;
import android.os.Handler;
import android.text.TextUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class MeeviiRequest {

    public interface RequsetCallback{
        void onFailure(String error);
        void onResponse(String response);
    }

    Context mContext = null;
    String mStrUrl = "";
    ArrayList<NameValuePair> mListParams = null;
    ArrayList<NameValuePair> mHeadParams = null;

    public MeeviiRequest(Context ctx, String strUrl){
        mContext = ctx;
        mStrUrl = strUrl;
    }

    public void addParam(String strKey, String strValue){
        if (mListParams == null)
            mListParams = new ArrayList<>();

        NameValuePair p = new NameValuePair(strKey, strValue);
        mListParams.add(p);
    }

    public void addHeader(String strKey, String strValue){
        if (mHeadParams == null)
            mHeadParams = new ArrayList<>();

        NameValuePair p = new NameValuePair(strKey, strValue);
        mHeadParams.add(p);
    }

    private void doRequest(Request request, final RequsetCallback callback){
        OkHttpClient.Builder builder = new OkHttpClient.Builder();


        builder.retryOnConnectionFailure(true)
                .connectTimeout(15, TimeUnit.SECONDS)
                .readTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS);

        OkHttpClient okHttpClient = builder.build();
        okHttpClient.newCall(request).enqueue(new Callback() {

            Handler mainHandler = new Handler(mContext.getMainLooper());
            String  mStrRet = "";
            @Override
            public void onFailure(Call call, IOException e) {
                if (callback != null){
                    final String strErr = e == null ? "unknown error" : e.toString();
                    mainHandler.post(new Runnable() {

                        @Override
                        public void run() {
                            callback.onFailure(strErr);
                        }
                    });

                }
            }

            @Override
            public void onResponse(Call call, Response response) {
                if (callback != null) {
                    try {
                        mStrRet = response.body().string();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    final int code = response.code();
                    mainHandler.post(new Runnable() {
                        @Override
                        public void run() {

                            if (code == 405){
                                callback.onFailure("not allow");
                            }else{
                                if (TextUtils.isEmpty(mStrRet)){
                                    callback.onResponse("body string error!");
                                }else{
                                    callback.onResponse(mStrRet);
                                }

                            }
                        }
                    });

                }
            }
        });
    }

    enum RequestMethod{
        Get,
        Post,
    }

    //url & header
    private Request.Builder createBuild(RequestMethod method){
        Request.Builder builder = new Request.Builder();
        if (method == RequestMethod.Get){
            String strUrl = makeGetUrl();
            builder.url(strUrl);
        }else{
            builder.url(mStrUrl);
        }

        if (mHeadParams != null){
            for (NameValuePair valuePair : mHeadParams) {
                builder.addHeader(valuePair.getName(), valuePair.getValue());
            }
        }

        if (method == RequestMethod.Get){
            builder.get();
        }else{
            FormBody.Builder body = new FormBody.Builder();
            if (mListParams != null){
                for (NameValuePair valuePair : mListParams) {
                    body.add(valuePair.getName(), valuePair.getValue());
                }
            }
            builder.post(body.build());
        }
        return builder;
    }


    public void Get(final RequsetCallback callback){
        Request request = createBuild(RequestMethod.Get).build();
        doRequest(request, callback);
    }


    public void Post(final RequsetCallback callback){
        Request request = createBuild(RequestMethod.Post).build();
        doRequest(request, callback);
    }

    private String makeGetUrl(){
        String strUrl = mStrUrl;

        StringBuilder content = new StringBuilder();
        if (mListParams != null){
            try {
                for (NameValuePair valuePair : mListParams) {
                    content.append('&')
                            .append(URLEncoder.encode(valuePair.getName(), "UTF-8"))
                            .append('=')
                            .append(URLEncoder.encode(valuePair.getValue(), "UTF-8"));
                }

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        if (content.length() > 0) {
            if (strUrl.indexOf('?') == -1)
                content.setCharAt(0, '?');
            strUrl += content.toString();
        }

        return strUrl;
    }


    /////////////////////
    public class NameValuePair {
        String strName = "";
        String strValue = "";

        public NameValuePair(String strName, String strValue){
            if (strName != null && strValue != null){
                this.strName = strName;
                this.strValue = strValue;
            }
        }

        public String getName(){
            return strName;
        }
        public String getValue(){
            return strValue;
        }

        public boolean equals(NameValuePair p){
            if (p == null)
                return false;
            return strName.equals(p.strName) && strValue.equals(p.strValue);
        }
    }
}
