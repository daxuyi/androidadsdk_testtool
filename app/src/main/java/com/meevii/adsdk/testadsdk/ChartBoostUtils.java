package com.meevii.adsdk.testadsdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.view.WindowManager;

import com.chartboost.sdk.Chartboost;

import java.lang.reflect.Field;

public class ChartBoostUtils {

    private static class CtxActivity extends Activity {

        Context applicationContext;
        public void setContext(Application application, Context applicationContext){
            setApplication(application);
            attachBaseContext(applicationContext);
            this.applicationContext = applicationContext;
        }

        private void setApplication(Application application){
            Field field = null;
            try {
                field = Activity.class.getDeclaredField("mApplication");
                field.setAccessible(true);
                field.set(this, application);
            } catch (NoSuchFieldException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        @Override
        public WindowManager getWindowManager() {
            WindowManager wm =(WindowManager)applicationContext.getSystemService(Context.WINDOW_SERVICE);
            return wm ;
        }

        @Override
        public Context getApplicationContext(){
            return applicationContext;
        }
    }

    public static void Initialize(Application application, Context applicationContext, String appId, String appSignature) {
        CtxActivity ctxActivity = new CtxActivity();
        ctxActivity.setContext(application, applicationContext);
        /*if (applicationContext instanceof Activity) {
            Log.i("chartBoost","Chartboost init appId:" + appId + " appSignature:" + appSignature);
            Chartboost.startWithAppId((Activity)applicationContext, appId, appSignature);

            Chartboost.onStart((Activity)applicationContext);
            return;
        }*/
//        Chartboost.onCreate(ctxActivity);
//        Chartboost.onStart(ctxActivity);
        Chartboost.startWithAppId(ctxActivity, appId, appSignature);
    }
    public static Activity getActivity(Application application, Context applicationContext){
        CtxActivity ctxActivity = new CtxActivity();
        ctxActivity.setContext(application, applicationContext);
        return ctxActivity;
    }
}
