package com.meevii.adsdk.testadsdk.httpserver;

import android.content.res.AssetManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;

/**
 * IO读取的工具类
 */
public class IOUtil {

    public static boolean copyAssetFile(AssetManager assetManager,
                                         String fromAssetPath, String toPath) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = assetManager.open(fromAssetPath);
            new File(toPath).createNewFile();
            out = new FileOutputStream(toPath);
            copyFile(in, out);
            return true;
        } catch(Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if(in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if(out != null) {
                try {
                    out.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static String getFileContext(File file){
        try {
            if (!file.exists()) {
                return "" ;
            }
            FileInputStream fis = new FileInputStream(file);
            InputStreamReader reader = new InputStreamReader(fis,"UTF-8");
            BufferedReader br = new BufferedReader(reader);
            if (fis == null)
                return "";
            String line ;
            StringBuffer sb = new StringBuffer();
            /*while ((line = br.readLine()) != null) {
                System.out.println(line);
                sb.append(line);
            }*/
            line = br.readLine();
            while (line != null) {
                sb.append(line);
                line = br.readLine();
                if (line != null) {
                    sb.append("\n");
                }
            }

            br.close();
            reader.close();
            fis.close();
            return sb.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return "" ;
    }

    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }
}
