package com.meevii.adsdk.testadsdk;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.httpserver.NanoHTTPDApi;
import com.meevii.adsdk.adsdk_lib.MeeviiADManager;
import com.meevii.adsdk.adsdk_lib.MeeviiADSDKSetting;
import com.meevii.adsdk.adsdk_lib.notify.ADPlatform;
import com.meevii.adsdk.testadsdk.httpserver.ConfigOperation;


public class MainActivity extends AppCompatActivity {

    enum LoadType{
        None,
        Local,
        OnLine,
    };
    enum LoadState{
        None,
        Loading,
        OK,
        Failed,
    }
    private static LoadType load_type = LoadType.None; //none
    private static LoadState load_state = LoadState.None;
    private ConfigOperation configOperation ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.load_local_cfg_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadLocalCfg();
            }
        });

        findViewById(R.id.edit_local_cfg_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editLocalCfg();
            }
        });

        findViewById(R.id.load_online_cfg_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadOnlineCfg();
            }
        });

        findViewById(R.id.next_scene_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                gotoBanner();
            }
        });

        enableADPlatform();

        onLoadStateChange();

        if (load_type == LoadType.None && load_state == LoadState.None)
            loadOnlineCfg();

        configOperation = new ConfigOperation(this.getApplicationContext());
        configOperation.migrationConfig();
    }

    private void enableADPlatform() {

        MeeviiADSDKSetting.EnableADPlatform(ADPlatform.AdMob, true);
        MeeviiADSDKSetting.EnableADPlatform(ADPlatform.FaceBook, true);
        MeeviiADSDKSetting.EnableADPlatform(ADPlatform.Unity, true);
        MeeviiADSDKSetting.EnableADPlatform(ADPlatform.AppLovin, true);
        MeeviiADSDKSetting.EnableADPlatform(ADPlatform.Vungle, true);
        MeeviiADSDKSetting.EnableADPlatform(ADPlatform.AdColony, true);
        MeeviiADSDKSetting.EnableADPlatform(ADPlatform.ChartBoost, true);
        MeeviiADSDKSetting.EnableADPlatform(ADPlatform.IronSource, true);
    }


    private void onLoadStateChange() {
        if (load_state == LoadState.Loading){
            if (load_type == LoadType.Local) showMsg("加载本地配置.....", Color.BLACK);
            else if (load_type == LoadType.OnLine) showMsg("加载在线配置....", Color.BLACK);
        }else if (load_state == LoadState.OK){
            if (load_type == LoadType.Local) showMsg("加载本地配置成功", Color.GREEN);
            else if (load_type == LoadType.OnLine) showMsg("加载在线配置成功", Color.GREEN);
        }else if (load_state == LoadState.Failed){
            if (load_type == LoadType.Local) showMsg("加载本地配置失败", Color.RED);
            else if (load_type == LoadType.OnLine) showMsg("加载在线配置失败", Color.RED);
        }else{
            showMsg("", Color.BLACK);
        }
    }

    private void loadLocalCfg() {

        if (load_type == LoadType.Local && load_state == LoadState.Loading)
            return;

        load_type = LoadType.Local;

        load_state = LoadState.Loading;
        onLoadStateChange();

        MeeviiADManager.Instance().UnInit();
        String configText = configOperation.getConfig();
        if (TextUtils.isEmpty(configText)) {
            configText = MeeviiADManager.Instance().GetSampleConfigFromStreamAssest(getApplicationContext());
        }

        MeeviiADManager.SetActivity(this);
        boolean bRet = MeeviiADManager.Instance().InitFromConfigString(getApplication(), getApplicationContext(), configText);
        if (bRet)
            load_state = LoadState.OK;
        else
            load_state = LoadState.Failed;

        onLoadStateChange();

    }

    private void editLocalCfg(){
        NanoHTTPDApi.getInstance(getApplicationContext()).setSavePath(configOperation.getDefaultFilePatch());
        String url = NanoHTTPDApi.getInstance(getApplicationContext()).startService();
        if (!TextUtils.isEmpty(url)) {
            showMsg("请用浏览器打开并配置：" + url, Color.GREEN);
        } else {
            showMsg("服务启动失败，请联系开发同事 ^_^", Color.RED);
        }
    }

    private void loadOnlineCfg() {
        if (load_type == LoadType.OnLine && load_state == LoadState.Loading)
            return;

        load_type = LoadType.OnLine;

        load_state = LoadState.Loading;
        onLoadStateChange();

        MeeviiADManager.Instance().UnInit();

        String appId = "adsdk.example.android";
        String strUrl = "http://testmatrix.dailyinnovation.biz/matrix/adconfig/getADConfig?app=" + appId + "&configVersion=0";

        //String appId = "bible.wordgame.words.connect.crossword.cookies";
        //String strUrl = "http://testmatrix.dailyinnovation.biz/matrix/adconfig/getADConfig?app=" + appId + "&configVersion=0";

        MeeviiRequest request = new MeeviiRequest(this, strUrl);

        request.addHeader("app", appId);
        request.addHeader("version", "0");
        request.addHeader("country", "US");
        request.addHeader("language", "EN");
        request.addHeader("versionNum", "0");
        request.addHeader("apiVersion", "1");
        request.addHeader("platform", "Android");
        //request.addHeader("user-agent", "Android");

        request.Get(new MeeviiRequest.RequsetCallback() {
            @Override
            public void onFailure(String error) {
                onGetOnlineData(false, "");
            }

            @Override
            public void onResponse(String response) {
                boolean bSuccess = false;
                if (response != null && response.length() > 0){
                    bSuccess = true;
                }
                onGetOnlineData(bSuccess, response);
            }
        });
    }

    private void onGetOnlineData(boolean bSuccess, String strData){
        if (load_type != LoadType.OnLine)
            return;

        if (!bSuccess){
            load_state = LoadState.Failed;
            onLoadStateChange();
        }else{

            MeeviiADManager.SetActivity(this);
            boolean bRet = MeeviiADManager.Instance().InitFromConfigString(getApplication(), getApplicationContext(), strData);
            if (bRet)
                load_state = LoadState.OK;
            else
                load_state = LoadState.Failed;

            onLoadStateChange();
        }
    }


    private void showMsg(String strText, int color) {
        TextView tv = (TextView)findViewById(R.id.msg_text);
        tv.setTextColor(color);
        tv.setText(strText);
    }

    private void gotoBanner() {
        startActivity(new Intent(this, BannerActivity.class));
    }

    @Override
    protected void onDestroy(){
        //MeeviiADManager.Instance().UnInit();
        super.onDestroy();
    }

    static long pressTime = 0;
    @Override
    public void onBackPressed(){
        long curTime = System.currentTimeMillis();
        if (curTime - pressTime > 3000){
            Toast.makeText(this, "再按一下退出app", Toast.LENGTH_SHORT).show();
            pressTime = curTime;
            return;
        }else{
            MeeviiADManager.Instance().UnInit();
            NanoHTTPDApi.getInstance(getApplicationContext()).stopService();
            finish();
            System.exit(0);
        }
        //super.onBackPressed();
    }
}
