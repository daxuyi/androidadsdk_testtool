package com.meevii.adsdk.testadsdk.httpserver;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

public class ConfigOperation {

    private Context content ;
    public static String fileName = "config";
    public static String assetsFileName = "test_ad.json";

    public ConfigOperation(Context context) {
        this.content = context;
    }

    public void migrationConfig(){
        final File file = getDefaultFile(content);

        if (file.exists()) {
            return;
        }
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                IOUtil.copyAssetFile(content.getAssets(), assetsFileName, file.getAbsolutePath());
            }
        }).start();
        return;
    }

    public String getConfig(){
        File file = getDefaultFile(content);
        if (file.exists())  {
            return IOUtil.getFileContext(file);
        }
        return "";
    }

    private File getDefaultFile(Context content){
        String dir = getDiskCacheDir(content);
        String filePath = dir + File.separator + fileName ;
        return new File(filePath) ;
    }

    public String getDefaultFilePatch(){
        return getDefaultFile(content).getAbsolutePath();
    }

    private String getDiskCacheDir(Context context) {
        String cachePath = null;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                || !Environment.isExternalStorageRemovable()) {

            File file = context.getExternalCacheDir();
            if (file != null) {
                cachePath = file.getPath();
            } else {
                if (context.getCacheDir() != null) {
                    cachePath = context.getCacheDir().getPath();
                }
            }
        } else {
            cachePath = context.getCacheDir().getPath();
        }
        return cachePath;
    }

}
