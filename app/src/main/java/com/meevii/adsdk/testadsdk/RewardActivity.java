package com.meevii.adsdk.testadsdk;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.meevii.adsdk.adsdk_lib.MeeviiADManager;
import com.meevii.adsdk.adsdk_lib.notify.IADGroupSet;
import com.meevii.adsdk.adsdk_lib.notify.IADGroupSetNotify;
import com.meevii.adsdk.adsdk_lib.notify.IADRewardNotify;
import com.meevii.adsdk.applovin_plugin.RewardVideo;

public class RewardActivity extends AppCompatActivity implements IADRewardNotify {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reward);

        findViewById(R.id.load_ad_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadAD();
            }
        });
        findViewById(R.id.show_ad_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAD();
            }
        });

        findViewById(R.id.pre_scene_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }

        });

        findViewById(R.id.next_scene_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

                Intent intent = new Intent();
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK |Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setClass(RewardActivity.this, MainActivity.class);
                startActivity(intent);
            }

        });

        if (TestIsCanShowAD()){
            showMsg("广告加载成功, 可以显示了", Color.GREEN);
        }
    }


    IADGroupSet getADGroupSet(){
        return MeeviiADManager.Instance().GetADGroupsetByID("reward01");
    }

    private void loadAD() {
        IADGroupSet groupSet = getADGroupSet();
        if (groupSet != null){
            showMsg("加载reward广告.....", Color.BLACK);
            groupSet.SetNotify(this);
            groupSet.StartRequest();
        }else{
            showMsg("配置文件没有reward广告!", Color.RED);
        }
    }

    private void showAD() {
        IADGroupSet groupSet = getADGroupSet();
        if (groupSet != null){
            if (groupSet.IsCanShowAD()){
                groupSet.ShowAD(this);
            }
        }
    }

    private boolean TestIsCanShowAD(){
        IADGroupSet groupSet = getADGroupSet();
        if (groupSet != null){
            if (groupSet.IsCanShowAD()){
                return true;
            }
        }
        return false;
    }

    protected void onDestroy(){
        DestroyAD();
        super.onDestroy();
    }


    private void showMsg(String strText, int color) {
        TextView tv = (TextView)findViewById(R.id.msg_text);
        tv.setTextColor(color);
        tv.setText(strText);
    }

    public void DestroyAD()
    {
        IADGroupSet adGroupSet = getADGroupSet();
        if (adGroupSet != null)
        {
            adGroupSet.CloseAD();
            adGroupSet.SetNotify(null);
        }
    }

    @Override
    public void OnAdLoad(String adTaskID) {
        IADGroupSet adGroupSet = getADGroupSet();
        if (adGroupSet != null)
        {
            showMsg("广告加载成功, 可以显示了", Color.GREEN);
            //if (adGroupSet.IsAutoDisplay() == false)
            //    adGroupSet.ShowAD();
        }
    }

    @Override
    public void OnAdLoadFailed() {
        showMsg("广告加载失败!", Color.RED);
    }

    @Override
    public void OnAdClicked(String adTaskID) {

    }

    @Override
    public void OnAdShow(String adTaskID) {

    }

    @Override
    public void OnAdClosed(String adTaskID) {

    }

    @Override
    public void OnAdLeavingApplication(String adTaskID) {

    }

    @Override
    public void OnRewarded(String adTaskID, Object data, String dataClazz) {

    }

    public void OnAdGroupLoad(String adTaskID){
    }

    @Override
    public void OnAdTryRefresh() {

    }
}
